package b2camp.timc.transactionservice.models.refference;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="m_transaction_code")
@Builder
public class MTransactionCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false,length = 50)
    private Long id;

    @Column(name = "transaction_code", nullable = false)
    private String transactionCode;

    @Column(name = "transaction_name", nullable = false)
    private String transactionName;

}
