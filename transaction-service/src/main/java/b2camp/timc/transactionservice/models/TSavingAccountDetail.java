package b2camp.timc.transactionservice.models;

import b2camp.timc.transactionservice.models.base.BaseEntity;
import b2camp.timc.transactionservice.models.refference.MTransactionCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_saving_account_detail")
@SuperBuilder
public class TSavingAccountDetail extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, length = 50)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "saving_account_id", referencedColumnName = "id")
    private TSavingAccount tSavingAccount;

    @Column(name = "mutation", nullable = false)
    private String mutation;

    @Column(name = "transaction_amount", nullable = false)
    private String transactionAmount;

    @ManyToOne
    @JoinColumn(name = "m_transaction_id", referencedColumnName = "id")
    private MTransactionCode mtransactionCode;

}
