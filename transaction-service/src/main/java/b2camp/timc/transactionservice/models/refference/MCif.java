package b2camp.timc.transactionservice.models.refference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "m_cif")
public class MCif {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 50)
    private Long id;

    @Column(name = "id_card", nullable = false, length = 100, unique = true)
    private String idCard;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "npwp", length = 100, unique = true)
    private String npwp;

    @Column(name = "no_telepon", nullable = false, length = 100, unique = true)
    private String noTelepon;

    @Column(name = "email", length = 100)
    private String email;

    @Column(nullable = false, length = 100)
    private String type;

}
