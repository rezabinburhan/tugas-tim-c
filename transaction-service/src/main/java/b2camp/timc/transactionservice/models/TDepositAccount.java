package b2camp.timc.transactionservice.models;

import b2camp.timc.transactionservice.models.base.BaseEntity;
import b2camp.timc.transactionservice.models.refference.MCif;
import b2camp.timc.transactionservice.models.refference.MDepositProduct;
import b2camp.timc.transactionservice.models.refference.RStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_deposit_account")
@SuperBuilder
public class TDepositAccount extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false,length = 50)
    private Long id;

    @OneToOne
    @JoinColumn(name = "t_saving_account_id")
    private TSavingAccount tSavingAccount;

    @Column(name = "nominal" , nullable = false)
    private BigDecimal nominal;

    @ManyToOne
    @JoinColumn(name = "cif_id",referencedColumnName = "id")
    private MCif mCif;

    @Column(name = "account_number",nullable = false)
    private String accountNumber;

    @ManyToOne
    @JoinColumn(name = "product_id",referencedColumnName = "id")
    private MDepositProduct mDepositProduct;

    @Column(name = "time_period", nullable = false)
    private Date timePeriod;

    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "due_date", nullable = false)
    private Date dueDate;

    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "id")
    private RStatus rStatus;
}
