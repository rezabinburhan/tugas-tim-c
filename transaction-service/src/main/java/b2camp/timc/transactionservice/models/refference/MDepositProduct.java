package b2camp.timc.transactionservice.models.refference;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="m_deposit_product")
@Builder
public class MDepositProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false,length = 50)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name= "interest_rate", nullable = false)
    private Double interestRate;

    @Column(name = "code", nullable = false)
    private String code;
}
