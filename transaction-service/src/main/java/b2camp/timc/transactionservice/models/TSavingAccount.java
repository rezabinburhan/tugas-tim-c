package b2camp.timc.transactionservice.models;

import b2camp.timc.transactionservice.models.base.BaseEntity;
import b2camp.timc.transactionservice.models.refference.MCif;
import b2camp.timc.transactionservice.models.refference.MSavingProduct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="t_saving_account")
@SuperBuilder
public class TSavingAccount extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false,length = 50)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "cif_id", referencedColumnName = "id")
    private MCif mcif;

    @ManyToOne
    @JoinColumn(name = "m_saving_product_id", referencedColumnName = "id")
    private MSavingProduct mSavingProduct;

    @Column(name = "account_number", unique = true, nullable = false)
    private String accountNumber;

    @Column(name = "balance", nullable = false)
    private BigDecimal balance;

}
