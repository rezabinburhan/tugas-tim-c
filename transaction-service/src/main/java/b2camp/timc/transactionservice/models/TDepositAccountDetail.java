package b2camp.timc.transactionservice.models;


import b2camp.timc.transactionservice.models.base.BaseEntity;
import b2camp.timc.transactionservice.models.refference.MTransactionCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="t_deposit_account_detail")
@SuperBuilder
public class TDepositAccountDetail extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false,length = 50)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "deposit_account_id",referencedColumnName = "id")
    private TDepositAccount tDepositAccount;

    @Column(name = "mutation", nullable = false)
    private String mutation;

    @Column(name = "transaction_amount", nullable = false)
    private String transactionAmount;


    @ManyToOne
    @JoinColumn(name = "transaction_code_id", referencedColumnName = "id")
    private MTransactionCode mtransactionCode;

}
