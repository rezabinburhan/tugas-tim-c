package b2camp.timc.transactionservice.models;

import b2camp.timc.transactionservice.models.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="m_deposit_bilyet")
@SuperBuilder
public class MDepositBilyet extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false,length = 50)
    private Long id;

    @Column(name = "bilyet_number",nullable = false,length = 50)
    private int bilYetNumber;


    @OneToOne(cascade = CascadeType.ALL, targetEntity = TDepositAccount.class)
    @JoinColumn(name = "t_deposit_account_id")
    private TDepositAccount tDepositAccount;

}
