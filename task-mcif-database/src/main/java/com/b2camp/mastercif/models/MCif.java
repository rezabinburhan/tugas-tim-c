package com.b2camp.mastercif.models;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "m_cif")
@Where(clause="deleted=false")
public class MCif {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 50)
    private Long id;

    @Column(name = "id_card", nullable = false, length = 100, unique = true)
    private String idCard;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "npwp", length = 100, unique = true)
    private String npwp;

    @Column(name = "no_telepon", nullable = false, length = 100, unique = true)
    private String noTelepon;

    @Column(name = "email", length = 100)
    private String email;

    @Column(nullable = false, length = 100)
    private String type;

    @Builder.Default
    private boolean deleted = Boolean.FALSE;


}
