package com.b2camp.mastercif.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "m_cif_family")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class MCifFamily {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 50)
    private Long id;

    @Column(name = "name", nullable = false, length = 200)
    private String name;

    @Column(name = "type", nullable = false, length = 200)
    private String type;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "created_on", columnDefinition = "DATE")
    @CreatedDate
    private Date createdOn;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;

    @Column(name = "last_modified_on", columnDefinition = "DATE")
    @LastModifiedDate
    private Date lastModifiedOn;

    private Boolean isDeleted;

    @PrePersist
    public void prePersist() {
        this.createdBy = getCreatedBy() != null ? getCreatedBy() : "SYSTEM";
        this.lastModifiedBy = getLastModifiedBy() != null ? getLastModifiedBy() : "SYSTEM";
    }

    @PreUpdate
    public void preModified() {
        this.lastModifiedBy = "SYSTEM MODIFIED";
    }

    @ManyToOne(fetch = FetchType.LAZY,targetEntity = MCif.class)
    @JoinColumn(name="mcif_id", referencedColumnName = "id")
    private MCif mCif;
}
