package com.b2camp.mastercif.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "m_cif_work")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MCifWork {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false)
    private Long id;

    @Column(name = "name",nullable = false,length = 200)
    private String name;

    @Column(name = "address",nullable = false,length = 200)
    private String address;

    @Column(name = "penghasilan",nullable = false,length = 200)
    private Double penghasilan;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "created_on", columnDefinition = "DATE")
    @CreatedDate
    private Date createdOn;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;

    @Column(name = "last_modified_on", columnDefinition = "DATE")
    @LastModifiedDate
    private Date lastModifiedOn;


    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @PrePersist
    public void prePersist(){
        this.createdBy = getCreatedBy()!=null?getCreatedBy():"SYSTEM";
        this.lastModifiedBy = getLastModifiedBy()!=null?getLastModifiedBy():"SYSTEM";
    }
    @PreUpdate
    public void preModified() {
        this.lastModifiedBy = "SYSTEM MODIFIED";
    }
    @ManyToOne(fetch = FetchType.LAZY,targetEntity = MCif.class)
    @JoinColumn(name="mcif_id", referencedColumnName = "id")
    private MCif mCif;

    }
