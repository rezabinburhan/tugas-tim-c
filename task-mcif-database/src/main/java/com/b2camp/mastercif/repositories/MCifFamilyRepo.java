package com.b2camp.mastercif.repositories;


import com.b2camp.mastercif.models.MCifFamily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MCifFamilyRepo extends JpaRepository<MCifFamily,Long> {
    @Query(value="SELECT * FROM m_cif_family mcf "
            + "WHERE mcf.id = :id "
            + "AND mcf.is_deleted = false", nativeQuery= true)
    MCifFamily findByIdTrue(@Param("id") Long id);

    @Query(value="SELECT m_cif from m_cif_family mcf" +
            "WHERE mcf.id = :id",nativeQuery = true)
    Long getMCifId(@Param("id") Long id);
}
