package com.b2camp.mastercif.repositories;

import com.b2camp.mastercif.models.MCifAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MCifAddressRepo extends JpaRepository<MCifAddress, Long> {
    @Query(value="SELECT * FROM m_cif_address mca "
            + "WHERE mca.id = :id "
            + "AND mca.is_deleted = false", nativeQuery= true)
    MCifAddress findByIdTrue(@Param("id") Long id);

    @Query(value="SELECT m_cif from m_cif_address mca" +
            "WHERE mca.id = :id",nativeQuery = true)
    Long getMCifId(@Param("id") Long id);

}
