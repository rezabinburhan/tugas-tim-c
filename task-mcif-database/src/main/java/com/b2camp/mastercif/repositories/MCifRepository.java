package com.b2camp.mastercif.repositories;


import com.b2camp.mastercif.models.MCif;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MCifRepository extends JpaRepository<MCif,Long> {
    @Modifying
    @Query(value="UPDATE public.m_cif SET deleted = true WHERE id = ?1",nativeQuery = true)
    void softDelete(Long id);

    @Modifying
    @Query(value="UPDATE public.m_cif set id = ?1 WHERE id = ?2",nativeQuery = true)
    void update(Long id1, Long id2 );
}
