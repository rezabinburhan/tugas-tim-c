package com.b2camp.mastercif.service.impl;



import com.b2camp.mastercif.dto.MCifRequest;
import com.b2camp.mastercif.dto.MCifResponse;
import com.b2camp.mastercif.models.MCif;
import com.b2camp.mastercif.repositories.MCifRepository;
import com.b2camp.mastercif.service.MCifService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@AllArgsConstructor
@Service
public class MCifServiceImpl implements MCifService {
    MCifRepository mCifRepository;

    @Transactional
    @Override
    public String insert(Integer type, MCifRequest request) {
        var checkDataNPWP = mCifRepository.findAll().stream().anyMatch(mCif -> mCif
                .getNpwp().equals(request.getNpwp()));
        var checkDataTelepon = mCifRepository.findAll().stream().anyMatch(mCif -> mCif
                .getNoTelepon().equals(request.getNoTelepon()));
        var checkDataKTP = mCifRepository.findAll().stream().anyMatch(mCif -> mCif
                .getIdCard().equals(request.getIdCard()));
        if (checkDataKTP == true || checkDataNPWP == true || checkDataTelepon == true) { //validasi unique
            return "Data already exist.";
        } else if (type.equals(1) && request.getNpwp().isBlank()) {
            return "Company needs NPWP data.";
        } else if (type.equals(1) || type.equals(2)) { // saya asumsikan 1 : Company, 2: Individu
            MCif data = MCif.builder()
                    .idCard(request.getIdCard())
                    .name(request.getName())
                    .npwp(request.getNpwp())
                    .noTelepon(request.getNoTelepon())
                    .email(request.getEmail())
                    .type(type.equals(1) ? "Company" : "Individual")
                    .build();

            MCif save = mCifRepository.save(data);

            MCifResponse response = MCifResponse.builder()
                    .id(save.getId())
                    .idCard(save.getIdCard())
                    .name(save.getName())
                    .npwp(save.getNpwp())
                    .noTelepon(save.getNoTelepon())
                    .email(save.getEmail())
                    .type(save.getType())
                    .build();

            return response + " Data Saved";
        } else {
            return "Customer type is not valid.";
        }
    }

    @Transactional
    @Override
    public String update(Long id, Integer type, MCifRequest request) {
        var checkData = mCifRepository.existsById(id);
        if (checkData == true) {
            if (type.equals(1) && request.getNpwp().isBlank()) {
                return "Company needs NPWP data.";
            } else if (type.equals(1) || type.equals(2)) {
                var backUp = mCifRepository.findById(id).get(); //backup data, jaga-jaga update gagal karena data sama
                MCif temporary = MCif.builder()                       //saat validasi di line 87
                        .id(id)
                        .idCard(backUp.getIdCard())
                        .name(backUp.getName())
                        .npwp(backUp.getNpwp())
                        .noTelepon(backUp.getNoTelepon())
                        .email(backUp.getEmail())
                        .type(backUp.getType())
                        .build();

                mCifRepository.deleteById(id); //hard delete agar saat validasi data tidak tetap ditemukan data yang sama

                var checkDataNPWP = mCifRepository.findAll().stream().anyMatch(mCif -> mCif
                        .getNpwp().equals(request.getNpwp()));
                var checkDataTelepon = mCifRepository.findAll().stream().anyMatch(mCif -> mCif
                        .getNoTelepon().equals(request.getNoTelepon()));
                var checkDataKTP = mCifRepository.findAll().stream().anyMatch(mCif -> mCif
                        .getIdCard().equals(request.getIdCard()));

                if (checkDataKTP == true || checkDataNPWP == true || checkDataTelepon == true) {
                    var autoIncrementId = mCifRepository.save(temporary); //data dikembalikan ke sebelum hard delete
                    mCifRepository.update(id, autoIncrementId.getId()); //update id kembali ke sebelumnya,
                    return "Data already exist.";             // karena kalau mengandalkan save saja,
                } else {                                      // id auto increment ke row setelahnya
                    MCif data = MCif.builder()
                            .idCard(request.getIdCard())
                            .name(request.getName())
                            .npwp(request.getNpwp())
                            .noTelepon(request.getNoTelepon())
                            .email(request.getEmail())
                            .type(type.equals(1) ? "Company" : "Individual")
                            .build();
                    MCif save = mCifRepository.save(data);
                    mCifRepository.update(id, save.getId());

                    MCifResponse response = MCifResponse.builder()
                            .id(id)
                            .idCard(save.getIdCard())
                            .name(save.getName())
                            .npwp(save.getNpwp())
                            .noTelepon(save.getNoTelepon())
                            .email(save.getEmail())
                            .type(save.getType())
                            .build();

                    return response + " Data Updated";
                }
            } else {
                return "Customer Type is not valid, 1 for company, 2 for individual";
            }
        }
        return "Given ID does not exists.";
    }

    @Transactional
    @Override
    public String softDelete(Long id) {
        var checkData1 = mCifRepository.existsById(id);
        var checkData2 = mCifRepository.findById(id).stream()
                .filter(mCif -> mCif.isDeleted()).equals(false);
        if(checkData1 == true) {
        if(checkData2 == false){
        mCifRepository.softDelete(id);
         return "Delete completed.";}}
        return "Given ID does not exists.";
    }

    @Transactional
    @Override
    public Iterable<MCif> findAll() {
        return mCifRepository.findAll();
    }

    @Transactional
    @Override
    public Optional<MCif> findById(Long id) {
        return Optional.ofNullable(mCifRepository.findById(id)).orElseThrow();
    }

}
