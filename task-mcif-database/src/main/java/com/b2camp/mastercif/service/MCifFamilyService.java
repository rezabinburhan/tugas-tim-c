package com.b2camp.mastercif.service;

import com.b2camp.mastercif.dto.MCifFamilyDto;

import java.util.Map;

public interface MCifFamilyService {
    Map<String, Object> create(Long id, MCifFamilyDto mCifFamilyDto);
    Map<String, Object> getAll();
    Map<String, Object> getById(Long id);
    Map<String, Object> update(Long id, MCifFamilyDto mCifFamilyDto);
    Map<String, Object> delete(Long id);
}
