package com.b2camp.mastercif.service;

import com.b2camp.mastercif.dto.MCifAddressDto;

import java.util.Map;

public interface MCifAddressService {
    Map<String, Object> create(Long id, MCifAddressDto mCifAddressDto);
    Map<String, Object> getAll();
    Map<String, Object> getById(Long id);
    Map<String, Object> update(Long id, MCifAddressDto mCifAddressDto);
    Map<String, Object> delete(Long id);

}
