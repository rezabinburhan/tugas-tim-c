package com.b2camp.mastercif.service;

import com.b2camp.mastercif.dto.MCifRequest;
import com.b2camp.mastercif.models.MCif;
import java.util.Optional;

public interface MCifService {
String insert(Integer type, MCifRequest request);
String update(Long id,Integer type, MCifRequest request);
String softDelete(Long id);
Iterable<MCif> findAll();
Optional<MCif> findById(Long id);
}
