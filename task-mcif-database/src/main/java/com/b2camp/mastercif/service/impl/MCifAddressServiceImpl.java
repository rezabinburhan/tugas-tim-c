package com.b2camp.mastercif.service.impl;

import com.b2camp.mastercif.constant.Constant;
import com.b2camp.mastercif.dto.MCifAddressDto;
import com.b2camp.mastercif.models.MCifAddress;
import com.b2camp.mastercif.repositories.MCifAddressRepo;
import com.b2camp.mastercif.repositories.MCifRepository;
import com.b2camp.mastercif.service.MCifAddressService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MCifAddressServiceImpl implements MCifAddressService {
    @Autowired
    MCifAddressRepo repository;

    @Autowired
    MCifRepository mCifRepository;

    ModelMapper mapper = new ModelMapper();

    private MCifAddress convertToEntity(MCifAddressDto mCifAddressDto) {
        return mapper.map(mCifAddressDto, MCifAddress.class);
    }

    private MCifAddressDto convertToDto(MCifAddress mCifAddress) {
        return mapper.map(mCifAddress, MCifAddressDto.class);
    }


    @Override
    public Map<String, Object> create(Long id, MCifAddressDto mCifAddressDto) {
        Map<String, Object> result = new HashMap<>();
        if (mCifRepository.existsById(id)) {
            MCifAddress mCifAddress = convertToEntity(mCifAddressDto);
            mCifAddress.setIsDeleted(false);
            mCifAddress.setMCif(mCifRepository.findById(id).get());

            MCifAddress resultInput = repository.save(mCifAddress);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, resultInput);
            return result;
        }
        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        return result;
    }

    @Override
    public Map<String, Object> getAll() {
        Map<String, Object> result = new HashMap<>();
        List<MCifAddressDto> listData = new ArrayList<>();

        for (MCifAddress data : repository.findAll()) {
            if (!data.getIsDeleted()) {
                MCifAddressDto mCifAddressDto = convertToDto(data);
                listData.add(mCifAddressDto);
            }
        }
        String message = "";
        if (!listData.isEmpty()) {
            message = Constant.SUCCESS_STRING;
        } else {
            message = Constant.EMPTY_DATA_STRING;
        }

        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, message);
        result.put(Constant.DATA_STRING, listData);
        result.put(Constant.TOTAL_STRING, listData.size());
        return result;
    }

    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        String message = "";
        try {
            MCifAddress data = repository.findByIdTrue(id);

            MCifAddressDto mCifAddressDto = convertToDto(data);
            message = Constant.SUCCESS_STRING;

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, message);
            result.put(Constant.DATA_STRING, mCifAddressDto);
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    //bisa juga digunakan generic class

    @Override
    public Map<String, Object> update(Long id, MCifAddressDto mCifAddressDto) {
        Map<String, Object> result = new HashMap<>();
        try {
            MCifAddress data = repository.findByIdTrue(id);
            MCifAddress mCifAddress = convertToEntity(mCifAddressDto);

            data.setName(mCifAddress.getName());
            data.setAddress(mCifAddress.getAddress());
            data.setMCif(mCifRepository.findById(repository.getMCifId(mCifAddressDto.getId())).get());

            MCifAddress resultInput = repository.save(data);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, resultInput);
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public Map<String, Object> delete(Long id) {
        Map<String, Object> result = new HashMap<>();
        try {
            MCifAddress data = repository.findByIdTrue(id);
            data.setIsDeleted(true);

            MCifAddress resultInput = repository.save(data);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, resultInput);
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }


}
