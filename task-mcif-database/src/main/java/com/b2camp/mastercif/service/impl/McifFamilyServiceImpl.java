package com.b2camp.mastercif.service.impl;

import com.b2camp.mastercif.constant.Constant;
import com.b2camp.mastercif.dto.MCifAddressDto;
import com.b2camp.mastercif.dto.MCifFamilyDto;
import com.b2camp.mastercif.models.MCifAddress;
import com.b2camp.mastercif.models.MCifFamily;
import com.b2camp.mastercif.repositories.MCifFamilyRepo;
import com.b2camp.mastercif.repositories.MCifRepository;
import com.b2camp.mastercif.service.MCifFamilyService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class McifFamilyServiceImpl implements MCifFamilyService {

    @Autowired
    MCifFamilyRepo repository;

    @Autowired
    MCifRepository mCifRepository;


    ModelMapper mapper = new ModelMapper();
    ;

    private MCifFamily convertToEntity(MCifFamilyDto mCifFamilyDto) {
        return mapper.map(mCifFamilyDto, MCifFamily.class);
    }

    private MCifFamilyDto convertToDto(MCifFamily mCifFamily) {
        return mapper.map(mCifFamily, MCifFamilyDto.class);
    }

    @Override
    public Map<String, Object> create(Long id, MCifFamilyDto mCifFamilyDto) {

        Map<String, Object> result = new HashMap<>();
        if (mCifRepository.existsById(id)) {
            MCifFamily mCifFamily = convertToEntity(mCifFamilyDto);
            mCifFamily.setIsDeleted(false);
            mCifFamily.setMCif(mCifRepository.findById(id).get());

            MCifFamily resultInput = repository.save(mCifFamily);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, resultInput);
            return result;
        }
        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        return result;
    }


    @Override
    public Map<String, Object> getAll() {
        Map<String, Object> result = new HashMap<>();
        List<MCifFamilyDto> listData = new ArrayList<>();

        for (MCifFamily dataFamily : repository.findAll()) {
            if (!dataFamily.getIsDeleted()) {
                MCifFamilyDto mCifFamilyDto = convertToDto(dataFamily);
                listData.add(mCifFamilyDto);
            }
        }
        String message = "";
        if (!listData.isEmpty()) {
            message = Constant.SUCCESS_STRING;
        } else {
            message = Constant.EMPTY_DATA_STRING;
        }

        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, message);
        result.put(Constant.DATA_STRING, listData);
        result.put(Constant.TOTAL_STRING, listData.size());
        return result;
    }

    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        String message = "";
        try {
            MCifFamily data = repository.findByIdTrue(id);

            MCifFamilyDto mCifFamilyDto = convertToDto(data);
            message = Constant.SUCCESS_STRING;

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, message);
            result.put(Constant.DATA_STRING, mCifFamilyDto);
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public Map<String, Object> update(Long id, MCifFamilyDto mCifFamilyDto) {
        Map<String, Object> result = new HashMap<>();
        try {
            MCifFamily data = repository.findByIdTrue(id);
            MCifFamily mCifFamily = convertToEntity(mCifFamilyDto);

            data.setName(mCifFamily.getName());
            data.setType(mCifFamily.getType());
            data.setMCif(mCifRepository.findById(repository.getMCifId(mCifFamilyDto.getId())).get());

            MCifFamily resultInput = repository.save(data);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, resultInput);
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public Map<String, Object> delete(Long id) {
        Map<String, Object> result = new HashMap<>();
        try {
            MCifFamily data = repository.findByIdTrue(id);
            data.setIsDeleted(true);

            MCifFamily resultInput = repository.save(data);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, resultInput);
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }
}
