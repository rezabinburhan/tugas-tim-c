package com.b2camp.mastercif.service;

import com.b2camp.mastercif.dto.MCifWorkDto;

import java.util.Map;

public interface MCifWorkService {

    Map<String, Object> create(Long id, MCifWorkDto mciCifWorkDto);
    Map<String, Object> getAll();
    Map<String, Object> getById(Long id);
    Map<String, Object> update(Long id, MCifWorkDto mCifWorkDto);
    Map<String, Object> delete(Long id);
}
