package com.b2camp.mastercif.dto;


import lombok.Data;

@Data
public class MCifRequest {
    private String idCard;
    private String name;
    private String npwp;
    private String noTelepon;
    private String email;
}
