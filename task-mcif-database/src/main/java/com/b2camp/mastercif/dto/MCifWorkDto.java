package com.b2camp.mastercif.dto;

import com.b2camp.mastercif.models.MCif;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class MCifWorkDto {

    private Long id;
    private String name;
    private String address;
    private Double penghasilan;
    private String createdBy;
    private Timestamp createdOn;
    private String lastModifiedBy;
    private Timestamp lastModifiedOn;
    private Boolean isDeleted;
    private List<MCif> mCif;
}
