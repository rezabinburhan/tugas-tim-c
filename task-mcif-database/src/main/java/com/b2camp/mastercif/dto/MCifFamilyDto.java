package com.b2camp.mastercif.dto;

import com.b2camp.mastercif.models.MCif;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
public class MCifFamilyDto {

    private Long id;
    private String name;
    private String type;
    private String createdBy;
    private Timestamp createdOn;
    private String lastModifiedBy;
    private Timestamp lastMmodifiedOn;
    private Boolean isDeleted;
    private List<MCif> mCif;
}
