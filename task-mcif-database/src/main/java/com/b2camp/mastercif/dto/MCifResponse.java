package com.b2camp.mastercif.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MCifResponse {
    private Long id;
    private String idCard;
    private String name;
    private String npwp;
    private String noTelepon;
    private String email;
    private String type;
}
