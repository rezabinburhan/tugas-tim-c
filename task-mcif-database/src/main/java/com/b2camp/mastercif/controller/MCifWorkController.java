package com.b2camp.mastercif.controller;


import com.b2camp.mastercif.dto.MCifWorkDto;
import com.b2camp.mastercif.service.MCifWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/mcifWork")
public class MCifWorkController {
    @Autowired
    private MCifWorkService service;


    @PostMapping("/create")
    public Map<String, Object> create(@RequestParam Long id,@RequestBody MCifWorkDto mCifWorkDto) {
        return service.create(id, mCifWorkDto);
    }

    @GetMapping("/getAll")
    public Map<String, Object> getAll() {
        return service.getAll();
    }

    @GetMapping("/getById")
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }

    @PutMapping("/update/{id}")
    public Map<String, Object> update(@PathVariable("id") Long id, @RequestBody MCifWorkDto mCifWorkDto) {
        return service.update(id, mCifWorkDto);
    }

    @DeleteMapping("/delete")
    public Map<String, Object> delete(@RequestParam("id") Long id) {
        return service.delete(id);
    }

}
