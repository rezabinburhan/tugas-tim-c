package com.b2camp.mastercif.controller;


import com.b2camp.mastercif.dto.MCifAddressDto;
import com.b2camp.mastercif.service.MCifAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/mcifAddress")
public class MCifAddressController {

    @Autowired
    private MCifAddressService service;

    @PostMapping("/create")
    public Map<String, Object> create(@RequestParam Long id, @RequestBody MCifAddressDto mCifAddressDto) {
        return service.create(id, mCifAddressDto);
    }

    @GetMapping("/getAll")
    public Map<String, Object> getAll() {
        return service.getAll();
    }

    @GetMapping("/getById")
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }

    @PutMapping("/update/{id}")
    public Map<String, Object> update(@PathVariable("id") Long id, @RequestBody MCifAddressDto mCifAddressDto) {
        return service.update(id, mCifAddressDto);
    }
    @DeleteMapping("/delete")
    public Map<String, Object> delete(@RequestParam("id") Long id) {
        return service.delete(id);
    }

}
