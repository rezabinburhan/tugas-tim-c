package com.b2camp.mastercif.controller;



import com.b2camp.mastercif.dto.MCifRequest;
import com.b2camp.mastercif.models.MCif;
import com.b2camp.mastercif.service.MCifService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@AllArgsConstructor
public class MCifController {

    MCifService mCifService;

    @GetMapping(value="/cif")
        public Iterable<MCif> findAll() {
            return mCifService.findAll();
    }

    @GetMapping(value="/cif/{id}")
    public Optional<MCif> findById(@PathVariable Long id) {
        return mCifService.findById(id);
    }

    @DeleteMapping(value="/cif/{id}")
    public String delete(@PathVariable Long id) {
        return mCifService.softDelete(id);

    }

    @PostMapping(value="/cif")
    public String insert(@RequestParam(name="type") Integer type, @RequestBody MCifRequest request) {
        return mCifService.insert(type, request);
    }
    @PutMapping(value="/cif/{id}/{type}")
    public String update(@PathVariable Long id,@RequestParam(name="type") Integer type, @RequestBody MCifRequest request){
        return mCifService.update(id,type,request);}

}
