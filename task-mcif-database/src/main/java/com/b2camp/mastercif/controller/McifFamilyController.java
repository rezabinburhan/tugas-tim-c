package com.b2camp.mastercif.controller;



import com.b2camp.mastercif.dto.MCifFamilyDto;
import com.b2camp.mastercif.service.MCifFamilyService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/mcifFamily")
public class McifFamilyController {

    @Autowired
    private MCifFamilyService service;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/create")
    public Map<String, Object> create(@RequestParam Long id,@RequestBody MCifFamilyDto mCifFamilyDto) {
        return service.create(id, mCifFamilyDto);
    }

    @GetMapping("/getAll")
    public Map<String, Object> getAll() {
        return service.getAll();
    }

    @GetMapping("/getById")
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }

    @PutMapping("/update/{id}")
    public Map<String, Object> update(@PathVariable("id") Long id, @RequestBody MCifFamilyDto mCifFamilyDto) {
        return service.update(id, mCifFamilyDto);
    }
    @DeleteMapping("/delete")
    public Map<String, Object> delete(@RequestParam("id") Long id) {
        return service.delete(id);
    }

}
