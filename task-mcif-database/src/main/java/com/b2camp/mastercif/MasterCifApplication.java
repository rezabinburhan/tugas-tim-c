package com.b2camp.mastercif;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@OpenAPIDefinition
@EnableJpaAuditing
public class MasterCifApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasterCifApplication.class, args);


	}
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
